# Building Bootloader

    west build -p -b adafruit_feather_nrf52840 -d build_mcuboot ../mcuboot/boot/zephyr/ -- -DDTC_OVERLAY_FILE=$(pwd)/overlay.dts -DOVERLAY_CONFIG=$(pwd)/mcuboot_overlay.conf

# Building Application

    west build -d build_app .

This will autmatically sign the firmware with `mcuboot/root-rsa-2048.pem`

# Test application

You can test the application by running it on your machine vaia the `native_posix_64` target in zephyr. Build the application by calling

    west build -b native_posix_64 -d build_x86 -- -DDTC_OVERLAY_FILE="" -DCONF_FILE=prj_x86.conf

and run it via `build_x86/zephyr/zephyr.exe`